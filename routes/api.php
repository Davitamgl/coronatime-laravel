<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StatisticController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\EmailVerificationController;
use App\Http\Controllers\Auth\PasswordResetLinkController;

Route::group(['middleware' => ['auth:sanctum']], function () {
	Route::post('/logout', [LoginController::class, 'destroy'])
	->name('logout');

	Route::get('/statistics', StatisticController::class)
	->name('statistics.get');
});

Route::post('/login', [LoginController::class, 'store'])
->name('login');

Route::post('/register', [RegisterController::class, 'store'])
->name('register');

Route::post('/email-verify/{token}/{email}', [EmailVerificationController::class, 'store'])
->name('verify.email');

Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
->name('password.mail');

Route::post('/reset-password', [NewPasswordController::class, 'store'])
->name('password.update');
