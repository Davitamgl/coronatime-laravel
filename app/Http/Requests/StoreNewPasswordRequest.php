<?php

namespace App\Http\Requests;

use App\Data\Auth\NewPasswordData;
use Spatie\LaravelData\WithData;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreNewPasswordRequest extends FormRequest
{
	use WithData;

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'email'                                 => ['required', 'email', 'exists:users'],
			'password'                              => ['required', 'confirmed', 'min:3', 'max:255'],
			'password_confirmation'                 => ['required'],
			'token'                                 => ['required'],
		];
	}

	public function failedValidation(Validator $validator)
	{
		throw new HttpResponseException(response()->json([
			'validation_error' => $validator->messages(),
		]));
	}

	protected function dataClass(): string
	{
		return NewPasswordData::class;
	}
}
