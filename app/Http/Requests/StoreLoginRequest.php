<?php

namespace App\Http\Requests;

use App\Data\Auth\LoginData;
use Spatie\LaravelData\WithData;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreLoginRequest extends FormRequest
{
	use WithData;

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'name'     => ['required'],
			'password' => ['required'],
		];
	}

	public function failedValidation(Validator $validator)
	{
		throw new HttpResponseException(response()->json([
			'validation_error' => $validator->messages(),
		]));
	}

	protected function dataClass(): string
	{
		return LoginData::class;
	}
}
