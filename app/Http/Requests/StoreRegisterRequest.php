<?php

namespace App\Http\Requests;

use App\Data\Auth\RegisterData;
use Spatie\LaravelData\WithData;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreRegisterRequest extends FormRequest
{
	use WithData;

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'name'                                  => ['required', 'min:3', 'max:255', 'unique:users'],
			'email'                                 => ['required', 'email', 'max:255', 'unique:users'],
			'password'                              => ['required', 'confirmed', 'min:3', 'max:255'],
			'password_confirmation'                 => ['required'],
		];
	}

	public function failedValidation(Validator $validator)
	{
		throw new HttpResponseException(response()->json([
			'validation_error' => $validator->messages(),
		]));
	}

	protected function dataClass(): string
    {
        return RegisterData::class;
    }
}
