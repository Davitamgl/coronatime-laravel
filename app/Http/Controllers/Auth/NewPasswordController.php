<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNewPasswordRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;

class NewPasswordController extends Controller
{
	public function store(StoreNewPasswordRequest $request)
	{
		$status = Password::reset(
			$request->getData()->all(),
			function ($user, $password) {
				$user->forceFill([
					'password' => Hash::make($password),
				])->setRememberToken(Str::random(60));
				$user->save();
				event(new PasswordReset($user));
			}
		);

		return $status == Password::PASSWORD_RESET
		?
			response()->json([
				'status'         => 200,
				'message'        => 'Password Successfully updated',
			], 200)
		:
		response()->json([
			'status'         => 422,
			'message'        => 'Password update failed.',
		]);
	}
}
