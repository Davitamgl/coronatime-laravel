<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePasswordResetLinkRequest;
use Illuminate\Support\Facades\Password;

class PasswordResetLinkController extends Controller
{
	public function store(StorePasswordResetLinkRequest $request)
	{
		Password::sendResetLink($request->only('email'));
		return response()->json([
			'status'         => 200,
			'message'        => 'Reset link sent to the email successfully',
		]);
	}
}
