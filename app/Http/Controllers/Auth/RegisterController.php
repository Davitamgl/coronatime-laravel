<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Mail\VerifyEmail;
use App\Models\VerifyUser;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\StoreRegisterRequest;

class RegisterController extends Controller
{
	public function store(StoreRegisterRequest $request)
	{
		$user = User::create($request->getData()->all());

		$user->createToken($request->device_name . '_Token')->plainTextToken;

		VerifyUser::create([
			'token'   => Str::random(100),
			'user_id' => $user->id,
		]);

		Mail::to($user->email)->send(new VerifyEmail($user, config('app.verify_email_url')));

		return response()->json([
			'status'         => 200,
			'message'        => 'User registered successfully, verification email sent',
		]);
	}
}
