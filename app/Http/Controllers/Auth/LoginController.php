<?php

namespace App\Http\Controllers\Auth;

use App\Data\UserData;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreLoginRequest;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
	public function store(StoreLoginRequest $request)
	{
		$credentials = $request->getData()->all();

		if (filter_var($credentials['name'], FILTER_VALIDATE_EMAIL))
		{
			$credentials['email'] = $credentials['name'];
			unset($credentials['name']);
		}

		if (Auth::attempt($credentials))
		{
			$user = Auth::getUser();
			$token = $user->createToken($request->device_name . '_Token')->plainTextToken;

			return response()->json([
				'status'         => 200,
				'user'           => UserData::from($user),
				'token'          => $token,
				'message'        => 'Logged in Succesfully',
			], 200);
		}
		else
		{
			return	response()->json([
				'status'  => 422,
				'message' => 'The provided credentials do not match our records.',
			]);
		}
	}

	public function destroy()
	{
		auth()->user()->tokens()->delete();
		return response()->json([
			'status' => 200,
			'message'=> 'Logged Out Successfully',
		]);
	}
}
