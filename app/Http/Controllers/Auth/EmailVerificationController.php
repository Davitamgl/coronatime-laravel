<?php

namespace App\Http\Controllers\Auth;

use App\Models\VerifyUser;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use App\Models\User;

class EmailVerificationController extends Controller
{
	public function store($token, $email)
	{
		$userExists = User::where('email', $email)->first();
		$verificationToken = VerifyUser::where('token', $token)->first();

		if (isset($verificationToken) && $userExists)
		{
			$user = $verificationToken->user;
			if (!$user->email_verified_at)
			{
				$user->email_verified_at = Carbon::now();
				$user->save();
				$verificationToken->delete();

				return response()->json([
					'status'         => 200,
					'message'        => 'User successfully verified.',
				]);
			}
		}
		elseif ($userExists)
		{
			if (strtotime($userExists->email_verified_at))
			{
				return response()->json([
					'status'         => 200,
					'message'        => 'User is already verified.',
				]);
			}
		}

		return response()->json([
			'status'         => 422,
			'message'        => 'Wrong verification token.',
		]);
	}
}
