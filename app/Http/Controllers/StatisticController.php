<?php

namespace App\Http\Controllers;

use App\Data\StatisticData;
use App\Models\Statistic;
use Illuminate\Support\Facades\DB;

class StatisticController extends Controller
{
	public function __invoke()
	{
		$sum = DB::table('statistics')
		->selectRaw('
		SUM(`confirmed`) as confirmed,
		SUM(`deaths`) as deaths,
		SUM(`recovered`) as recovered')
		->get();

		return response()->json([
			'status'         => 200,
			'sum'             => $sum,
			'statistics'      => StatisticData::collection(Statistic::paginate(6)),
		]);
	}
}
