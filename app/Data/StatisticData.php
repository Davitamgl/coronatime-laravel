<?php

namespace App\data;

use App\Models\Statistic;
use Spatie\LaravelData\Data;

class StatisticData extends Data
{
	public function __construct(
		public int $id,
		public ?int $confirmed,
		public ?int $recovered,
		public ?int $deaths,
		public array $country,
	) {
	}

	public static function fromModel(Statistic $statistic): self
	{
		return new self(
			id:			$statistic->id,
			confirmed:	$statistic->confirmed,
			recovered:	$statistic->recovered,
			deaths:		$statistic->deaths,
			country:	$statistic->getTranslations('country', ['en', 'ka']),
		);
	}
}
