<?php

namespace App\data\Auth;

use Spatie\LaravelData\Data;

class LoginData extends Data
{
	public function __construct(
		public string $name,
		public string $password,
	) {
	}
}
