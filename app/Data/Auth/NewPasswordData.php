<?php

namespace App\data\Auth;

use Spatie\LaravelData\Data;

class NewPasswordData extends Data
{
	public function __construct(
		public string $email,
		public string $password,
		public string $password_confirmation,
		public string $token,
	) {
	}
}
