<?php

namespace App\data;

use App\Models\User;
use Spatie\LaravelData\Data;
use Illuminate\Support\Carbon;

class UserData extends Data
{
	public function __construct(
		public string $name,
		public Carbon|null $email_verified,
	) {
	}

	public static function fromModel(User $user): self
	{
		return new self(
			$user->name,
			$user->email_verified_at,
		);
	}
}
