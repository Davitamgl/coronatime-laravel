<?php

namespace App\Console\Commands;

use App\Models\Statistic;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class FetchStatistics extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'fetch:statistics';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'fetchs statistics from api and store into the database';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle()
	{
		$countryList = Http::get('https://devtest.ge/countries')->json();

		$progressCount = 1;
		foreach ($countryList as $country)
		{
			$statistics = Http::post('https://devtest.ge/get-country-statistics', ['code' => $country['code']])->json();
			Statistic::updateOrCreate(
				['country_code'   => $statistics['code']],
				[
					'country'               => $country['name'],
					'confirmed'             => $statistics['confirmed'],
					'recovered'             => $statistics['recovered'],
					'deaths'                => $statistics['deaths'],
				]
			);

			$this->info('IN PROGRESS' . ': ' . $progressCount . '/' . count(($countryList)));
			$progressCount++;
		}
		$this->info('Data Fetched Succesfully' . '   :   ' . now());
	}
}
