@component('mail::message')

@if (App::environment('production'))
<img src={{config('app.url').'/images/worldwide.png'}} alt="worldwide">
@elseif (App::environment('local'))
<img src="{{ asset('/images/worldwide.png') }}" />
@endif

<h1 style="margin-top: 30px">
    <center>   
    Confirmation email
</center> 
</h1> 
<center>click this button to verify your email</center>
@component('mail::button', ['url' => $url . '/'. $user->verifyUser->token.'/'.$user->email, 'color' => 'green'])
VERIFY EMAIL
@endcomponent


@endcomponent