@component('mail::message')

@if (App::environment('production'))
<img src={{config('app.url').'/images/worldwide.png'}} alt="worldwide">
@elseif (App::environment('local'))
<img src="{{ asset('/images/worldwide.png') }}" />
@endif   

<h1 style="margin-top: 30px">
    <center>   
    Recover password
</center> 
</h1> 
<center>click this button to recover a password</center>
@component('mail::button', ['url' => $url . '/'. $token . '/'.$email, 'color' => 'green'])
RECOVER PASSWORD
@endcomponent


@endcomponent
