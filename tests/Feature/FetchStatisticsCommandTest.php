<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use App\Models\Statistic;
use Illuminate\Support\Facades\Http;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FetchStatisticsCommandTest extends TestCase
{
	use RefreshDatabase;

	public function test_console_command_fetch_statistics_fetches_data_and_stores_into_database()
	{
		$fakeCountryList = [
			[
				'code' => 'YE',
				'name' => [
					'en' => 'Yemen',
					'ka' => 'იემენი',
				],
			],
			[
				'code' => 'ZW',
				'name' => [
					'en' => 'Zimbabwe',
					'ka' => 'ზიმბაბვე',
				],
			],
		];
		$fakeCountryStatistics = [
			[
				'id'        => 1,
				'country'   => 'Yemen',
				'code'      => 'YE',
				'confirmed' => 100,
				'recovered' => 200,
				'deaths'    => 300,
				'created_at'=> '2021-09-13T11:43:29.000000Z',
				'updated_at'=> '2021-09-13T11:43:29.000000Z',
			],
			[
				'id'        => 2,
				'country'   => 'Zimbabwe',
				'code'      => 'ZW',
				'confirmed' => 200,
				'recovered' => 400,
				'deaths'    => 600,
				'created_at'=> '2021-09-13T11:43:39.000000Z',
				'updated_at'=> '2021-09-13T11:43:39.000000Z',
			],
		];
		Http::fake(
			[
				'https://devtest.ge/countries' => Http::response($fakeCountryList),
			],
		);

		$this->artisan('fetch:statistics');

		$countries = [];
		$countryList = Http::get('https://devtest.ge/countries')->json();
		$sequence = Http::sequence();

		foreach ($fakeCountryStatistics as $value)
		{
			$sequence->push($value);
		}
		Http::fake(
			[
				'https://devtest.ge/get-country-statistics' => $sequence,
			],
		);

		foreach ($countryList as $key => $element)
		{
			$countryStatistics = Http::post('https://devtest.ge/get-country-statistics', $element)->json();

			$country = [
				'name'      => $element['name'],
				'code'      => $element['code'],
				'confirmed' => $countryStatistics['confirmed'],
				'recovered' => $countryStatistics['recovered'],
				'deaths'    => $countryStatistics['deaths'],
			];
			array_push($countries, $country);
		}
		foreach ($countries as $country)
		{
			$entry = Statistic::where('country_code', '=', $country['code'])->first();
			if ($entry === null)
			{
				Statistic::create(
					[
						'country_code'          => $country['code'],
						'country'               => [
							'en' => $country['name']['en'],
							'ka' => $country['name']['ka'],
						],
						'confirmed' => $country['confirmed'],
						'recovered' => $country['recovered'],
						'deaths'    => $country['deaths'],
					],
				);
			}
			else
			{
				$attributes = [
					'confirmed' => $country['confirmed'],
					'recovered' => $country['recovered'],
					'deaths'    => $country['deaths'],
				];
				$entry->update($attributes);
			}
		}
		$this->assertDatabaseHas('statistics', [
			'country_code'      => 'YE',
			'confirmed'         => 100,
			'recovered'         => 200,
			'deaths'            => 300,
		]);
		$this->assertDatabaseHas('statistics', [
			'country_code'      => 'ZW',
			'confirmed'         => 200,
			'recovered'         => 400,
			'deaths'            => 600,
		]);
	}

	public function test_fetch_statistics_updates_data_if_data_already_exists_in_database()
	{
		Statistic::create([
			'country'               => 'Yemen',
			'country_code'          => 'YE',
			'confirmed'             => 1,
			'recovered'             => 2,
			'deaths'                => 3,
		]);
		Statistic::create([
			'country'               => 'Zimbabwe',
			'country_code'          => 'ZW',
			'confirmed'             => 1,
			'recovered'             => 2,
			'deaths'                => 3,
		]);

		$fakeCountryList = [
			[
				'code' => 'YE',
				'name' => [
					'en' => 'Yemen',
					'ka' => 'იემენი',
				],
			],
			[
				'code' => 'ZW',
				'name' => [
					'en' => 'Zimbabwe',
					'ka' => 'ზიმბაბვე',
				],
			],
		];
		$fakeCountryStatistics = [
			[
				'id'        => 1,
				'country'   => 'Yemen',
				'code'      => 'YE',
				'confirmed' => 100,
				'recovered' => 200,
				'deaths'    => 300,
				'created_at'=> '2021-09-13T11:43:29.000000Z',
				'updated_at'=> '2021-09-13T11:43:29.000000Z',
			],
			[
				'id'        => 2,
				'country'   => 'Zimbabwe',
				'code'      => 'ZW',
				'confirmed' => 200,
				'recovered' => 400,
				'deaths'    => 600,
				'created_at'=> '2021-09-13T11:43:39.000000Z',
				'updated_at'=> '2021-09-13T11:43:39.000000Z',
			],
		];
		Http::fake(
			[
				'https://devtest.ge/countries' => Http::response($fakeCountryList),
			],
		);

		$this->artisan('fetch:statistics');

		$countries = [];
		$countryList = Http::get('https://devtest.ge/countries')->json();
		$sequence = Http::sequence();

		foreach ($fakeCountryStatistics as $value)
		{
			$sequence->push($value);
		}
		Http::fake(
			[
				'https://devtest.ge/get-country-statistics' => $sequence,
			],
		);

		foreach ($countryList as $key => $element)
		{
			$countryStatistics = Http::post('https://devtest.ge/get-country-statistics', $element)->json();

			$country = [
				'name'      => $element['name'],
				'code'      => $element['code'],
				'confirmed' => $countryStatistics['confirmed'],
				'recovered' => $countryStatistics['recovered'],
				'deaths'    => $countryStatistics['deaths'],
			];
			array_push($countries, $country);
		}
		foreach ($countries as $country)
		{
			$entry = Statistic::where('country_code', '=', $country['code'])->first();
			if ($entry === null)
			{
				Statistic::create(
					[
						'country_code'          => $country['code'],
						'country'               => [
							'en' => $country['name']['en'],
							'ka' => $country['name']['ka'],
						],
						'confirmed' => $country['confirmed'],
						'recovered' => $country['recovered'],
						'deaths'    => $country['deaths'],
					],
				);
			}
			else
			{
				$attributes = [
					'confirmed' => $country['confirmed'],
					'recovered' => $country['recovered'],
					'deaths'    => $country['deaths'],
				];
				$entry->update($attributes);
			}
		}
		$this->assertDatabaseHas('statistics', [
			'country_code'      => 'YE',
			'confirmed'         => 100,
			'recovered'         => 200,
			'deaths'            => 300,
		]);
		$this->assertDatabaseHas('statistics', [
			'country_code'      => 'ZW',
			'confirmed'         => 200,
			'recovered'         => 400,
			'deaths'            => 600,
		]);
	}
}
