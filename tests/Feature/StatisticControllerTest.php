<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use App\Models\User;
use App\Models\Statistic;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StatisticControllerTest extends TestCase
{
	use RefreshDatabase;

	/** @test */
	public function test_statistics_route_returns_json()
	{
		$user = User::factory()->create();
		$token = $user->createToken('_Token')->plainTextToken;
		$response = $this->withHeader('Authorization', 'Bearer ' . $token)
		->get(route('statistics.get'));
		$response->assertStatus(200);
		$response->assertSee('sum', 'statistics', 'confirmed', 'deaths', 'recovered');
		json_decode($response->getContent());
		if (json_last_error() != JSON_ERROR_NONE)
		{
			abort(400, 'Bad JSON received');
		}
	}

	/** @test */
	public function test_statistics_route_returns_correct_data_from_database()
	{
		$user = User::factory()->create();
		$token = $user->createToken('_Token')->plainTextToken;

		Statistic::create([
			'country'               => 'Georgia',
			'country_code'          => 'GE',
			'confirmed'             => 1001,
			'recovered'             => 2002,
			'deaths'                => 3003,
		]);
		$this->withHeader('Authorization', 'Bearer ' . $token)
		->get(route('statistics.get'))
		->assertJsonFragment([
			'confirmed'=> '1001',
			'deaths'   => '3003',
			'recovered'=> '2002',
		]);
	}
}
