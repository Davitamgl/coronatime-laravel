<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegistrationTest extends TestCase
{
	use RefreshDatabase;

	/** @test */
	public function test_new_user_can_register()
	{
		$response = $this->post(route('register'), [
			'name'                  => 'Test User',
			'email'                 => 'test@example.com',
			'password'              => 'password',
			'password_confirmation' => 'password',
		]);

		$response->assertSessionHasNoErrors();
	}

	/** @test */
	public function test_existing_user_cant_register()
	{
		User::factory()->create([
			'name'     => 'user',
			'email'    => 'test@example.com',
			'password' => 'secret',
		]);
		$response = $this->post(route('register'), [
			'name'                  => 'Test User',
			'email'                 => 'test@example.com',
			'password'              => 'password',
			'password_confirmation' => 'password',
		]);

		$response->assertSee('validation_error', 'The email has already been taken.');
	}

	/** @test */
	public function test_register_validation_errors_render()
	{
		$response = $this->post(route('register'), []);
		$response->assertSee([
			'name'                  => 'The name field is required.',
			'email'                 => 'The email field is required.',
			'password'              => 'The password field is required.',
			'password_confirmation' => 'The password confirmation field is required.',
		]);
	}

	public function test_registration_name_has_already_taken__error_renders()
	{
		User::factory()->create([
			'name'     => 'user',
			'email'    => 'test@example.com',
			'password' => 'secret',
		]);
		$response = $this->post(route('register'), [
			'name'                  => 'user',
			'email'                 => 'test@example.com',
			'password'              => 'password',
			'password_confirmation' => 'password',
		]);
		$response->assertSee(['name' => 'The name has already been taken.']);
	}

	/** @test */
	public function test_registration_name_min_error_renders()
	{
		$response = $this->post(route('register'), [
			'name'                  => 'T',
			'email'                 => 'test@example.com',
			'password'              => 'password',
			'password_confirmation' => 'password',
		]);
		$response->assertSee(['name' => 'The name must be at least 3 characters.']);
	}

	/** @test */
	public function test_registration_name_max_error_renders()
	{
		$response = $this->post(route('register'), [
			'name'                  => 'wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww',
			'email'                 => 'test@example.com',
			'password'              => 'password',
			'password_confirmation' => 'password',
		]);
		$response->assertSee(['name' => 'The name must not be greater than 255 characters.']);
	}

	public function test_registration_email_has_already_taken__error_renders()
	{
		User::factory()->create([
			'name'     => 'user',
			'email'    => 'test@example.com',
			'password' => 'secret',
		]);
		$response = $this->post(route('register'), [
			'name'                  => 'user',
			'email'                 => 'test@example.com',
			'password'              => 'password',
			'password_confirmation' => 'password',
		]);
		$response->assertSee(['email' => 'The email has already been taken.']);
	}

	/** @test */
	public function test_registration_password_required_error_renders()
	{
		$response = $this->post(route('register'), [
			'name'                  => 'user',
			'email'                 => 'test@example.com',
			'password'              => '',
			'password_confirmation' => 'password',
		]);

		$response->assertSee(['password' => 'The password field is required.']);
	}

	/** @test */
	public function test_registration_password_min_error_renders()
	{
		$response = $this->post(route('register'), [
			'name'                  => 'user',
			'email'                 => 'test@example.com',
			'password'              => '1',
			'password_confirmation' => '',
		]);
		$response->assertSee(['password' => 'The password must be at least 3 characters.']);
	}

	/** @test */
	public function test_registration_password_max_error_renders()
	{
		$response = $this->post(route('register'), [
			'name'                  => 'user',
			'email'                 => 'test@example.com',
			'password'              => 'wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww',
			'password_confirmation' => 'password',
		]);
		$response->assertSee(['password' => 'The password must not be greater than 255 characters.']);
	}

	/** @test */
	public function test_registration_password_confirmation_error_renders()
	{
		$response = $this->post(route('register'), [
			'name'                  => 'user',
			'email'                 => 'test@example.com',
			'password'              => '123',
			'password_confirmation' => 'password',
		]);

		$response->assertSee(['password' => 'The password confirmation does not match.']);
	}
}
