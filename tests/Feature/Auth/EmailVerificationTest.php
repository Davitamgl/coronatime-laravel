<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use App\Models\User;
use App\Models\VerifyUser;
use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmailVerificationTest extends TestCase
{
	use RefreshDatabase;

	/** @test */
	public function test_email_can_be_verified()
	{
		$user = User::factory()->unverified()->create();
		VerifyUser::create([
			'token'   => Str::random(100),
			'user_id' => $user->id,
		]);

		$this->post(route('verify.email', [
			'token' => $user->verifyUser->token,
			'email' => $user->email,
		]));

		$this->assertTrue($user->fresh()->email_verified_at != null);
	}

	/** @test */
	public function test_email_cant_be_verified_with_wrong_token()
	{
		$user = User::factory()->unverified()->create();

		VerifyUser::create([
			'token'   => Str::random(100),
			'user_id' => $user->id,
		]);

		$this->post(route('verify.email', [
			'token' => 'wrong_token',
			'email' => $user->email,
		]));

		$this->assertTrue($user->fresh()->email_verified_at == null);
	}

	/** @test */
	public function test_already_email_verified_users_are_not_verified_again()
	{
		$user = User::factory()->create();

		VerifyUser::create([
			'token'   => Str::random(100),
			'user_id' => $user->id,
		]);
		$response = $this->post(route('verify.email', [
			'token' => 'token',
			'email' => $user->email,
		]));
		$response->assertSee([
			'message'                  => 'User is already verified.',
		]);
	}
}
