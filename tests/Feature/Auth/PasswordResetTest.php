<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use App\Notifications\CustomResetPasswordNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class PasswordResetTest extends TestCase
{
	use RefreshDatabase;

	/** @test */
	public function test_reset_password_link_can_be_requested()
	{
		Notification::fake();
		$user = User::factory()->create();
		$this->post(route('password.mail'), ['email' => $user->email]);
		Notification::assertSentTo($user, CustomResetPasswordNotification::class);
	}

	/** @test */
	public function test_reset_password_link_can_not_be_requested_with_unregistered_email()
	{
		Notification::fake();
		User::factory()->create();
		$response = $this->post(route('password.mail'), ['email' => 'wrong-email']);
		$response->assertSee([
			'email'                  => 'The email must be a valid email address.',
		]);
	}

	/** @test */
	public function test_password_can_be_reset_with_valid_token()
	{
		Notification::fake();
		$user = User::factory()->create();
		$this->post(route('password.mail'), ['email' => $user->email]);
		Notification::assertSentTo($user, CustomResetPasswordNotification::class, function ($notification) use ($user) {
			$response = $this->post(route('password.update'), [
				'token'                 => $notification->token,
				'email'                 => $user->email,
				'password'              => 'password',
				'password_confirmation' => 'password',
			]);
			$response->assertSessionHasNoErrors();
			return true;
		});
	}

	/** @test */
	public function test_reset_password_throws_validation_errors()
	{
		Notification::fake();
		$user = User::factory()->create();
		$this->post(route('password.mail'), ['email' => $user->email]);
		Notification::assertSentTo($user, CustomResetPasswordNotification::class, function ($notification) use ($user) {
			$response = $this->post(route('password.update'), []);
			$response->assertSee([
				'token'                  => 'The token field is required.',
				'email'                  => 'The email field is required.',
				'password'               => 'The password field is required.',
				'password_confirmation'  => 'The password confirmation field is required.',
			]);
			return true;
		});
	}

	/** @test */
	public function test_password_wont_update_with_wrong_token()
	{
		Notification::fake();
		$user = User::factory()->create();
		$this->post(route('password.mail'), ['email' => $user->email]);
		Notification::assertSentTo($user, CustomResetPasswordNotification::class, function ($notification) use ($user) {
			$response = $this->post(route('password.update'), [
				'token'                 => 'wrong_token',
				'email'                 => $user->email,
				'password'              => 'password',
				'password_confirmation' => 'password',
			]);
			$response->assertSee([
				'message'                  => 'Password update failed.',
			]);
			return true;
		});
	}
}
