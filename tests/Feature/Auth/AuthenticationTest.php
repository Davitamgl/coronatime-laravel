<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthenticationTest extends TestCase
{
	use RefreshDatabase;

	/** @test */
	public function test_users_can_authenticate()
	{
		$user = User::factory()->create();

		$response = $this->post(route('login'), [
			'name'     => $user->email,
			'password' => 'password',
		]);
		$response->assertSessionHasNoErrors();
		$this->assertAuthenticated();
	}

	/** @test */
	public function test_users_can_authenticate_using_username()
	{
		$user = User::factory()->create();

		$response = $this->post(route('login'), [
			'name'     => $user->name,
			'password' => 'password',
		]);

		$this->assertAuthenticated();
	}

	/** @test */
	public function test_users_can_not_authenticate_with_invalid_password()
	{
		$user = User::factory()->create();

		$response = $this->post(route('login'), [
			'name'     => $user->email,
			'password' => 'wrong-password',
		]);
		$response->assertSee(['message' => 'The provided credentials do not match our records.']);
	}

	/** @test */
	public function test_auth_token_is_screated_when_user_log_in()
	{
		$this->assertDatabaseMissing('personal_access_tokens', [
			'id'      => 1,
		]);
		$user = User::factory()->create();

		$this->post(route('login'), [
			'name'     => $user->email,
			'password' => 'password',
		]);
		$this->assertDatabaseHas('personal_access_tokens', [
			'id'      => 1,
		]);
	}

	/** @test */
	public function test_login_response_has_auth_token_and_verification_status()
	{
		$user = User::factory()->create();

		$response = $this->post(route('login'), [
			'name'     => $user->email,
			'password' => 'password',
		]);
		$response->assertSee(
			'name',
			'token',
			'email_verified'
		);
	}

	/** @test */
	public function test_login_validation_errors_render()
	{
		$response = $this->post(route('login'), []);
		$response->assertSee([
			'name'                  => 'The name field is required.',
			'password'              => 'The password field is required.',
		]);
	}

	/** @test */
	public function test_user_can_logout()
	{
		$user = User::factory()->create();

		$response = $this->post(route('login'), [
			'name'     => $user->name,
			'password' => 'password',
		]);

		$this->assertAuthenticated();
		$response = $this->post(route('logout'));
		$response->assertStatus(200);
	}
}
