<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ScheduleTaskTest extends TestCase
{
	use RefreshDatabase;

	/** @test */
	public function test_schedule_runs()
	{
		$this->artisan('schedule:run')->assertSuccessful();
	}
}
