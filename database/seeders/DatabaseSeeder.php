<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Statistic;
use App\Models\Statistics;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		User::Create([
			'name'              => 'test',
			'email'             => 'test@gmail.com',
			'email_verified_at' => now(),
			'password'          => Hash::make('test'),
		]);
		Statistic::create([
			'country'               => 'Afghanistan',
			'country_code'          => 'AF',
			'confirmed'             => 50600,
			'recovered'             => 20500,
			'deaths'                => 40400,
		]);
		Statistic::create([
			'country'               => 'Georgia',
			'country_code'          => 'GE',
			'confirmed'             => 50100,
			'recovered'             => 50200,
			'deaths'                => 50300,
		]);
		Statistic::create([
			'country'               => 'Turkey',
			'country_code'          => 'TR',
			'confirmed'             => 40500,
			'recovered'             => 20600,
			'deaths'                => 10700,
		]);
	}
}
