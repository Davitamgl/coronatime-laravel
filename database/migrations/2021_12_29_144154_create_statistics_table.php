<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatisticsTable extends Migration
{
	public function up()
	{
		Schema::create('statistics', function (Blueprint $table) {
			$table->id();
			$table->string('country')->unique();
			$table->string('country_code')->unique();
			$table->integer('confirmed');
			$table->integer('recovered');
			$table->integer('deaths');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('statistics');
	}
}
