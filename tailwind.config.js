module.exports = {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
    ],
    theme: {
        extend: {
            colors: {
                dark4: "#F6F6F7",
                dark60: "#808189",
                dark100: "#010414",
                svggray: "#BFC0C4",
                brandblue: "#2029F3",
                brandgreen: "#0FBA68",
                brandyellow: "#EAD621",
                bgblue: "#EEEEFE",
                bggreen: "#ECFAF3",
                bgyellow: "#FDFCEE",
            },
            maxWidth: {
                151: "37.75rem",
                esm: "21.4375rem",
            },
            height: {
                225: "56.25rem",
                150: "37.5rem",
                30: "40vh",
                50: "50vh",
                mobile: "calc(100vh - 378px)",

            },
            width: {
                22.5: "5.625rem",
                160: "40rem",
                209: "52.25rem",
            },
            fontSize: {
                xf: ["25px", "30px"],
                mdd: ["20px", "24.2px"],
            },
            gap: {
                0.5: "0.2rem",
            },
            margin: {
                "5px": "5px",
            },
            screens: {
                esm: "500px",
                // => @media (min-width: 500px) { ... }
            },
            maxHeight: {
                31: "50vh",
            },
        },
    },
    plugins: [],
};
