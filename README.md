<h2>
Api Endpoint for Coronatime
</h2>

<div style="display:flex; align-items: center">

  <h1 style="position:relative; top: -6px" >CoronaTime</h1>
 
</div>

   !["CI / CD"](./README/projectImage.png)


### Table of Contents
- [Table of Contents](#table-of-contents)
- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
- [Database Diagram](#database-diagram)
- [Api Documentation](#api-documentation)
- [Development](#development)
#
### Prerequisites

* [composer@2.1.9](https://getcomposer.org/)
* [php@8.0.11](https://www.php.net/)
* [npm@8.1.0](https://nodejs.org/en/download/)






### Getting Started
1\. First of all you need to clone CoronaTime repository from github:
```sh
git clone https://github.com/Davitamgl/coronatime-laravel.git
```

2\. Next step requires you to run *composer install* in order to install all the dependencies.
```sh
composer install
```

3\. after you have installed all the PHP dependencies, it's time to install all the JS dependencies:
```sh
npm install
```

and also:
```sh
npm run dev
```
in order to build your JS/SaaS resources.

4\. Now we need to set our env file. Go to the root of your project and execute this command.
```sh
cp .env.example .env
```
5\. Now execute in the root of you project following:
```sh
php artisan key:generate
```
Which generates auth key.

\And now you should provide **.env** file all the necessary environment variables:
#
**APP_URL:**
>APP_URL=http://127.0.0.1:8000 <br/>
>FRONTEND_URL_VERIFY_EMAIL=http://localhost:3000/email-verify <br/>
>FRONTEND_URL_RESET_PASSWORD=http://localhost:3000/reset-password <br/>
#
make sure you have set the those urls othervise mailing systems wont work

#
**DB_CONNECTION:**
>DB_CONNECTION=sqlite
#
#
**FILESYSTEM_DRIVER:**
>FILESYSTEM_DRIVER=public
#
#
**QUEUE_CONNECTION:**
>QUEUE_CONNECTION =database
#

**MAILING_SYSTEM:**
>MAIL_MAILER=smtp <br>
>MAIL_HOST=mailhog <br>
>MAIL_PORT=1025 <br>
>MAIL_USERNAME=null <br>
>MAIL_PASSWORD=null <br>
>MAIL_ENCRYPTION=null <br>
>MAIL_FROM_ADDRESS=null <br>
>MAIL_FROM_NAME="${APP_NAME}" <br>
#
use the mailing system you prefer, I suggest to use mailtrap for development 

after setting up **.env** file, execute:

```sh
php artisan storage:link
```
to create a symbolic link from public/storage to storage/app/public.

```sh
touch database/database.sqlite
```
to create sqlite database file.

and then

```sh
php artisan config:cache
```
if you want to cache environment variables.

Now, you should be good to go!

#
### Migration
if you've completed getting started section, then migrating database if fairly simple process, just execute:
```sh
php artisan migrate
```
next, you gonna make the workers work, otherwise emails won't be sent to addressants
```sh
php artisan queue:work
```



If you want to Run generate some gibberish  data run

```sh
php artisan migrate:fresh --seed
```
To fetch some data from api run 
```sh
php artisan fetch:statistics
```

#
### Api Documentation
You can see Api Design and Documentation by visiting route /swagger, for example

```sh
http://localhost:8000/swagger
```
!["CI / CD"](./README/swagger.png)
#

#
### Database Diagram
[Database Design Diagram](https://drawsql.app/redberry-12/diagrams/coronatime)

!["CI / CD"](./README/CoronaTime-drawSQL.png)




#
### Development

You can run Laravel's built-in development server by executing:

```sh
  php artisan serve
```


#
### Test User
you can access project dashboard with test user, but you should have run database seeder to create test user

name=test <br>
email=test@gmail.com <br>
password=test <br>


